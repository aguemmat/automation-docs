= Service Execution (REST API) Endpoint

_Accept: application/json, application/json+metadata, application/json+result, application/xml, text/html, application/pdf_

_Content-Type: application/json, application/xml, text/html_

_HTTP-Methods: GET, POST (raw encoding)_

This endpoint allows the execution of services through an HTTP POST REST call. Using the GET verb, a template of the expected input format will be returned.

== Retrieving the Template (HTTP GET)

Making a REST call using that method allows to obtain a template on the expected format to execute this service.

Depending on the value of the Accept header, an appropriate template will be generated:

=== application/json

A JSON data example is returned that contains the input requirement of the service.

 { 
    "customer": { 
        "first name": "", 
        "last name": "", 
        "birthday": "",        
        "email": "", 
        "address": { 
            "street": "", 
            "number": 0, 
            "state": "" 
        } 
    } 
 } 


=== application/xml

An XML data example is returned that contains the input requirement of the service. This XML format is greatly inspired by the link:https://github.com/dmn-tck/tck[DMN TCK] format.

 <!doctype html>
 <html>
   <body>
      <form enctype="multipart/form-data" method="POST" action="https://instance.trisotech.com/execution/bpmn/api/run/test/bpmn/bpmn-customer">
         <label for="customer-0">customer.first name</label><input type="text" id="customer-0" name="customer.first name">
         <br><label for="customer-1">customer.last name</label><input type="text" id="customer-1" name="customer.last name">
         <br><label for="customer-2">customer.birthday</label><input type="date" id="customer-2" name="customer.birthday">
         <br><label for="customer-3">customer.email</label><input type="text" id="customer-3" name="customer.email">
         <br><label for="customer-4">customer.address.street</label><input type="text" id="customer-4" name="customer.address.street">
         <br><label for="customer-5">customer.address.number</label>
         <input type="number" id="customer-5" name="customer.address.number"><br><label for="customer-6">customer.address.state</label>
         <select name="customer.address.state" id="input-6" size="1">
            <option value="Alberta">Alberta</option>
            <option value="Bristish-Columbia">Bristish-Columbia</option>
            <option value="Manitoba">Manitoba</option>
            <option value="New Brunswick">New Brunswick</option>
            <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
            <option value="Nova Scotia">Nova Scotia</option>
            <option value="Ontario">Ontario</option>
            <option value="Prince Edward Island">Prince Edward Island</option>
            <option value="Quebec">Quebec</option>
            <option value="Sakatchewan">Sakatchewan</option>
         </select>
         <br><input type="submit" value="Run">
      </form>
   </body>
 </html>


== Executing the Service (HTTP POST)

Making a REST call using that method start the execution of the service.

The _Accept_ header specifies the rendering format of the response sent back by the service and the _Content-Type_ header will specify the request body format (both default to application/json). If the _Content-Type_ header is not set, the request will be handled with a body's encoding format based on the _Accept_ header's value. So, if a POST is made with a body and no Content-Type but Accept is set, the body format will be assumed to be the same as in the Accept header.

=== JSON Accepts Headers

The endpoint support two JSON output format depending on your requirements for the output. The goal is to either just provide the result or provide the result plus additional meta-data. This could be useful if you want to create a service that respect a given JSON schema for its output because when using _application/json+result_, the output is controlled by your model completely. 

|===
|Accept Header |Format description

|_application/json_
|The administrator can configure the default return when not specifying the sub-format. Default to: _application/json+metadata_

|_application/json+metadata_
|The returned JSON contains only the results and all the meta-data (instance identifier, error messages, status, tags, resume points, ...)

|_application/json+result_
|The returned JSON contains only the result of the execution, some of the meta-data is returned as headers
|===
