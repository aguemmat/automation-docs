= Sidebar

The left side bar of the tool provides three features. It's allows you to access to the xref:user-menu.adoc[User Menu], to the xref:preferences-menu.adoc[Preferences Menu] and the xref:about.adoc[product information menu].

image::sidebar.png[]