= Test Endpoint

_Content-Type_: _application/xml_

_HTTP-Methods_: POST _(raw encoding)_

This endpoint executes test cases defined in the service at modeling time or submitted through the HTTP POST body (raw encoded). External test cases are defined using the DMN TCK format (https://github.com/dmn-tck/tck). The XSD for the test case is available here: https://raw.githubusercontent.com/dmn-tck/tck/master/TestCases/testCases.xsd.
 
For a usage example of this endpoint with DMN services, you can look at the DMN TCK test runner from Trisotech: https://github.com/dmn-tck/tck/tree/master/runners/dmn-tck-runner-trisotech.

If no XML file are posted on the call, the test cases defined in the model will be used.

The resulting XML file is simple and will look like:

 <?xml version="1.0" encoding="UTF-8" standalone="no"?>

 <testResult xmlns="http://www.omg.org/spec/DMN/20160719/testcase">
  <testCase id="test-1" name="Test 1" passed="true"/>
  <testCase id="test-2" name="Test 2" passed="false"/>
    …
 </testResult>

The resulting XML contains one _testCase_ element for each submitted test case and will return a passed/failed result for each.
