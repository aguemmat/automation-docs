= About Menu

The about menu allows you to quickly glance at the basic information about the Kommunicator; its current version, your current subscriptions as well as their expiry dates and other information.

== Using the "About Menu" in the left side bar
 
Navigate to the btn:[About menu] icon and click on it. 
 image:about-menu-icon.png[]

Then the following modal will appear.

image::about-menu.png[]