include::_@Modelers:Knowledge-Entity-Modeler:partial$nav-product.adoc[]

include::_@Modelers:Workflow-Modeler:partial$nav-product.adoc[]

include::_@Modelers:Case-Modeler:partial$nav-product.adoc[]

include::_@Modelers:Decision-Modeler:partial$nav-product.adoc[]

include::_@automation:ideation:partial$nav.adoc[]

include::_@automation:service-library:partial$nav.adoc[]

include::_@automation:cloud-execution:partial$nav.adoc[]

include::_@automation:system-integration:partial$nav.adoc[]







